﻿using System.Collections.Generic;
using System.Text;

namespace it.unibo.pacman.model.mapeditor
{

	using EntityType = it.unibo.pacman.model.utilities.EntityType;

	public class FixedGridImpl : IFixedGrid
	{

		private readonly int rows;
		private readonly int columns;
		private readonly IList<IList<GridEntity>> map;

		public FixedGridImpl(int rows, int columns)
		{
			this.rows = rows;
			this.columns = columns;
			this.map = this.createEmptyMap();
		}

		private IList<IList<GridEntity>> createEmptyMap()
		{
			IList<IList<GridEntity>> tmpMap = new List<IList<GridEntity>>();
			this.fillMap(tmpMap, EntityType.EMPTY);
			return tmpMap;
		}
	
		private void fillMap(IList<IList<GridEntity>> mapToFill, EntityType type)
		{
			for (int y = 0; y < this.rows; y++)
			{
				mapToFill.Add(new List<GridEntity>());
				for (int x = 0; x < this.columns; x++)
				{
					mapToFill[y].Add(new GridEntity(type, true));
				}
			}
		}

		public virtual void fillWithPills()
		{
			for (int y = 0; y < this.rows; y++)
			{
				for (int x = 0; x < this.columns; x++)
				{
					if (this.getEntity(x, y).Equals(EntityType.EMPTY))
					{
						this.setEntity(x, y, EntityType.PILL);
					}
				}
			}
		}

		public virtual void setEntity(int x, int y, EntityType type)
		{
			if (this.isPositionFixed(x, y))
			{
				// Always modifiable.
				this.map[y][x] = new GridEntity(type, true);
			}
		}

		public virtual void setFixedEntity(int x, int y, EntityType type)
		{
			if (this.isPositionFixed(x, y))
			{
				this.map[y][x] = new GridEntity(type, false);
			}
		}

		public bool isPositionFixed(int x, int y)
		{
			return this.map[y][x].Fixed;
		}

		public virtual EntityType getEntity(int x, int y)
		{
			return this.map[y][x].Type;
		}

		public virtual int Rows
		{
			get
			{
				return this.rows;
			}
		}

		public virtual int Columns
		{
			get
			{
				return this.columns;
			}
		}

		public IList<IList<GridEntity>> Map
		{
			get
			{
				return this.map;
			}
		}

		public override string ToString()
		{
			StringBuilder sb = new StringBuilder(30);
			sb.Append("Columns= ").Append(this.columns).Append("\nRows= ").Append(this.rows).Append('\n');
			for (int y = 0; y < this.rows; y++)
			{
				for (int x = 0; x < this.columns; x++)
				{
					sb./*Append('(').Append(y).Append(',').Append(x).Append(')').*/Append(this.map[y][x].Type.ToString()).Append("\t");
				}
				sb.Append('\n');
			}
			return sb.ToString();
		}


		public sealed class GridEntity
		{

			internal readonly EntityType type;
			internal readonly bool @fixed;

			public GridEntity(EntityType type, bool @fixed)
			{
				this.type = type;
				this.@fixed = @fixed;
			}

			public EntityType Type
			{
				get
				{
					return type;
				}
			}

			public bool Fixed
			{
				get
				{
					return @fixed;
				}
			}

		}

	}

}