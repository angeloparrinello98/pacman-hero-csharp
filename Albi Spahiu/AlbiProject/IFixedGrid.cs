﻿using System.Collections.Generic;

namespace it.unibo.pacman.model.mapeditor
{

	using GridEntity = it.unibo.pacman.model.mapeditor.FixedGridImpl.GridEntity;
	using EntityType = it.unibo.pacman.model.utilities.EntityType;

	public interface IFixedGrid : IGrid
	{

		void setFixedEntity(int x, int y, EntityType type);

		bool isPositionFixed(int x, int y);

		void fillWithPills();

		IList<IList<GridEntity>> Map {get;}

	}

}