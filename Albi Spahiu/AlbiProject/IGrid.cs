﻿namespace it.unibo.pacman.model.mapeditor
{
	using EntityType = it.unibo.pacman.model.utilities.EntityType;

	public interface IGrid
	{

		void setEntity(int x, int y, EntityType type);

		EntityType getEntity(int x, int y);

		int Rows {get;}

		int Columns {get;}

	}

}