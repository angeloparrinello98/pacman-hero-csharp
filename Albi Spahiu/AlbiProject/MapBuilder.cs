﻿namespace it.unibo.pacman.model.mapeditor
{

	using Position = it.unibo.pacman.model.utilities.Position;
	using EntityType = it.unibo.pacman.model.utilities.EntityType;

	public class MapBuilder
	{

		private readonly int rows;
		private readonly int columns;
		private readonly int centerX;
		private readonly int centerY;
		private readonly Position pacmanInitialPos;
		private readonly Position pinkyInitialPos;
		private readonly Position blinkyInitialPos;
		private readonly Position inkyInitialPos;
		private readonly Position clydeInitialPos;

		private readonly FixedGridImpl grid;

		public MapBuilder(int rows, int columns)
		{
			grid = new FixedGridImpl(rows, columns);
			this.rows = rows;
			this.columns = columns;
			this.centerX = (columns - 1) / 2;
			this.centerY = (rows - 1) / 2;
			this.pacmanInitialPos = new Position(this.centerX, this.centerY + 2);
			this.pinkyInitialPos = new Position(this.centerX, this.centerY - 1);
			this.blinkyInitialPos = new Position(this.centerX, this.centerY);
			this.inkyInitialPos = new Position(this.centerX - 1, this.centerY);
			this.clydeInitialPos = new Position(this.centerX + 1, this.centerY);
		}

		public virtual MapBuilder addFixedBordersWithPortals()
		{
			this.addFixedBorders();
			this.addFixedPortals();
			return this;
		}

		private void addFixedBorders()
		{
			for (int y = 1; y < rows - 1; y++)
			{
				if (y != this.centerY)
				{
					grid.setFixedEntity(0, y, EntityType.WALL);
					grid.setFixedEntity(this.columns - 1, y, EntityType.WALL);
				}
			}
			for (int x = 0; x < this.columns; x++)
			{
				grid.setFixedEntity(x, 0, EntityType.WALL);
				grid.setFixedEntity(x, this.rows - 1, EntityType.WALL);
			}
		}

		private void addFixedPortals()
		{
			for (int xLeft = 0; xLeft < 3; xLeft++)
			{
				grid.setFixedEntity(xLeft, this.centerY - 1, EntityType.WALL);
				grid.setFixedEntity(xLeft, this.centerY, EntityType.EMPTY);
				grid.setFixedEntity(xLeft, this.centerY + 1, EntityType.WALL);
			}
			for (int xRight = this.columns - 3; xRight < this.columns; xRight++)
			{
				grid.setFixedEntity(xRight, this.centerY - 1, EntityType.WALL);
				grid.setFixedEntity(xRight, this.centerY, EntityType.EMPTY);
				grid.setFixedEntity(xRight, this.centerY + 1, EntityType.WALL);
			}
		}

		public virtual MapBuilder addFixedCenter()
		{
			grid.setFixedEntity(this.pacmanInitialPos.X, this.pacmanInitialPos.Y, EntityType.PACMAN);
			grid.setFixedEntity(this.pinkyInitialPos.X, this.pinkyInitialPos.Y, EntityType.PINKY);
			grid.setFixedEntity(this.blinkyInitialPos.X, this.blinkyInitialPos.Y, EntityType.BLINKY);
			grid.setFixedEntity(this.inkyInitialPos.X, this.inkyInitialPos.Y, EntityType.INKY);
			grid.setFixedEntity(this.clydeInitialPos.X, this.clydeInitialPos.Y, EntityType.CLYDE);
			for (int y = this.centerY - 1; y <= this.centerY; y++)
			{
				grid.setFixedEntity(this.centerX - 2, y, EntityType.WALL);
				grid.setFixedEntity(this.centerX + 2, y, EntityType.WALL);
			}
			for (int x = this.centerX - 2; x <= this.centerX + 2; x++)
			{
				grid.setFixedEntity(x, this.centerY + 1, EntityType.WALL);
			}
			for (int y = this.centerY - 2; y <= this.centerY + 2; y++)
			{
				for (int x = this.centerX - 3; x <= this.centerX + 3; x++)
				{
					if (grid.isPositionFixed(x, y))
					{
						grid.setFixedEntity(x, y, EntityType.EMPTY);
					}
				}
			}
			return this;
		}

		public virtual FixedGridImpl Grid
		{
			get
			{
				return grid;
			}
		}

	}

}