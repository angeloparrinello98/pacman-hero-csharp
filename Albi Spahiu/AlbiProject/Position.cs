﻿namespace it.unibo.pacman.model.utilities
{

	public class Position
	{
		private readonly int x;
		private readonly int y;

		public Position(int x, int y)
		{
			this.x = x;
			this.y = y;
		}

		public int X
		{
			get
			{
				return x;
			}
		}

		public int Y
		{
			get
			{
				return y;
			}
		}
		public override sealed int GetHashCode()
		{
			const int prime = 31;
			int result = 1;
			result = prime * result + x;
			result = prime * result + y;
			return result;
		}

		public override sealed bool Equals(object obj)
		{
			if (this == obj)
			{
				return true;
			}
			if (obj == null)
			{
				return false;
			}
			if (this.GetType() != obj.GetType())
			{
				return false;
			}

			Position other = (Position) obj;
			if (x != other.x)
			{
				return false;
			}
			return y == other.y;
		}

		public override sealed string ToString()
		{
			return "Position [x=" + x + ", y=" + y + "]";
		}
	}

}