﻿using it.unibo.pacman.model.mapeditor;
using it.unibo.pacman.model.utilities;
using System;

namespace AlbiProject
{

    using Position = it.unibo.pacman.model.mapeditor.MapBuilder;

    class Test
    {
        static void Main(string[] args)
        {
            FixedGridImpl map = new MapBuilder(19, 17).addFixedBordersWithPortals().addFixedCenter().Grid;
            Console.WriteLine(map.ToString());

            map.setEntity(0, 0, EntityType.POWERPILL); //fixed: doesn't change the type at the given position
            map.setEntity(1, 1, EntityType.POWERPILL);
            map.setEntity(2, 2, EntityType.WALL);
            map.setEntity(3, 3, EntityType.WALL);
            map.setEntity(4, 4, EntityType.WALL);
            map.setEntity(5, 5, EntityType.WALL);
            map.setEntity(6, 6, EntityType.WALL);
            map.setEntity(7, 7, EntityType.WALL); //fixed: doesn't change the type at the given position
            map.setEntity(8, 8, EntityType.WALL); //fixed: doesn't change the type at the given position
            map.setEntity(9, 9, EntityType.WALL); //fixed: doesn't change the type at the given position
            map.setEntity(10, 10, EntityType.WALL); //fixed: doesn't change the type at the given position
            map.setEntity(11, 11, EntityType.WALL); //fixed: doesn't change the type at the given position
            map.setEntity(12, 12, EntityType.WALL);
            map.setEntity(13, 13, EntityType.WALL);
            Console.WriteLine(map.ToString());

            map.fillWithPills();
            Console.WriteLine(map.ToString());
        }
    }
}
