﻿using System.Drawing;

namespace Ghost
{
    interface IEntity
    {
        Rectangle Bounds { get; }

        Position Position { get; }

        EntityType Type { get; }
    }
}
