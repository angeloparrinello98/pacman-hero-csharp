﻿using System.Drawing;

namespace Ghost
{
    interface IMovable : IEntity
    {
		Position Position { get; set; }

		Direction Direction { get; set; }

		Status Status { get; set; }

		Rectangle getBoundsAt(Position position);

		Position nextPosition();

	}
}
