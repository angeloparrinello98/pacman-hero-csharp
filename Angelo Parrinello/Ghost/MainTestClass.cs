﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ghost
{
    class MainTestClass
    {
        static void Main(string[] args)
        {
            IEntity entity = new SimpleGameObj(10, 10, new Position(10, 10), EntityType.BLINKY);
            Console.WriteLine(entity.Position);
            Console.WriteLine(entity.Type);
            Console.WriteLine(entity.Bounds);

           IMovable movable = new MovableGameObj(10, 10, new Position(10, 10), EntityType.CLYDE, Direction.DOWN, Status.CHASING, 2);
           Console.WriteLine(movable.Position);
           Console.WriteLine(movable.Direction);
           movable.Position = movable.nextPosition();
           Console.WriteLine(movable.Position);
           movable.Direction = Direction.LEFT;
           Console.WriteLine(movable.Direction);
           movable.Position = movable.nextPosition();
           Console.WriteLine(movable.Position);

           Console.WriteLine(Direction.getOppositeDirection(movable.Direction));
           movable.Direction = Direction.RandomDirection;
           Console.WriteLine(movable.Direction);
           movable.Position = movable.nextPosition();
           Console.WriteLine(movable.Position);

        }

    }
}