﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiacomoProject
{
    class Direction
    {
        public static readonly Direction LEFT = new Direction("LEFT", InnerEnum.LEFT);

        public static readonly Direction RIGHT = new Direction("RIGHT", InnerEnum.RIGHT);

        public static readonly Direction UP = new Direction("UP", InnerEnum.UP);

        public static readonly Direction DOWN = new Direction("DOWN", InnerEnum.DOWN);

        private static readonly List<Direction> valueList = new List<Direction>();

        static Direction()
        {
            valueList.Add(LEFT);
            valueList.Add(RIGHT);
            valueList.Add(UP);
            valueList.Add(DOWN);
        }

        public enum InnerEnum
        {
            LEFT,
            RIGHT,
            UP,
            DOWN
        }

        public readonly InnerEnum innerEnumValue;
        private readonly string nameValue;
        private readonly int ordinalValue;
        private static int nextOrdinal = 0;

        private Direction(string name, InnerEnum innerEnum)
        {
            nameValue = name;
            ordinalValue = nextOrdinal++;
            innerEnumValue = innerEnum;
        }
        private static readonly Direction[] VALUES = values();

        public static IList<Direction> DirectionList
        {
            get
            {
                return new List<Direction>(Direction.values());
            }
        }

        public static Direction RandomDirection
        {
            get
            {
                return VALUES[(new Random()).Next(3)];
            }
        }

        public override string ToString()
        {
            switch (this.innerEnumValue)
            {
                case InnerEnum.UP:
                    return "Up";
                case InnerEnum.DOWN:
                    return "Down";
                case InnerEnum.LEFT:
                    return "Left";
                case InnerEnum.RIGHT:
                    return "Right";
                default:
                    return "";
            }
        }

        public static Direction getOppositeDirection(Direction dir)
        {
            switch (dir.innerEnumValue)
            {
                case InnerEnum.UP:
                    return DOWN;
                case InnerEnum.DOWN:
                    return UP;
                case InnerEnum.LEFT:
                    return RIGHT;
                case InnerEnum.RIGHT:
                    return LEFT;
                default:
                    return null;
            }
        }


        public static Direction[] values()
        {
            return valueList.ToArray();
        }

        public int ordinal()
        {
            return ordinalValue;
        }

        public static Direction valueOf(string name)
        {
            foreach (Direction enumInstance in Direction.valueList)
            {
                if (enumInstance.nameValue == name)
                {
                    return enumInstance;
                }
            }
            throw new System.ArgumentException(name);
        }
    }
}
