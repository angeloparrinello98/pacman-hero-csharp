﻿using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiacomoProject
{
    interface IEntity
    {
        Rectangle Bounds { get; }

        Position Position { get; }

        EntityType Type { get; }
    }
}
