﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiacomoProject
{
    interface IMortal : IMovable
    {
        bool Dead { get; }

        void decreaseLives();

        int Lives { get; }
    }
}
