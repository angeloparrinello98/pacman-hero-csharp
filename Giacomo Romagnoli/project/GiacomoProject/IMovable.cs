﻿using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiacomoProject
{
    interface IMovable : IEntity
    {
        Position Position { get; set; }

        Direction Direction { get; set; }

        Status Status { get; set; }

        Rectangle getBoundsAt(Position position);

        Position nextPosition();

    }
}
