﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiacomoProject
{
    class MainTests
    {
        static void Main(string[] args)
        {
            IMortal pacman = new MortalGameObj(30, 30, new Position(20, 20), EntityType.PACMAN, Direction.LEFT, Status.CHASED, 2, 3);
            //test posizioni
            System.Console.WriteLine(pacman.Position);
            pacman.Position = pacman.nextPosition();
            System.Console.WriteLine(pacman.Position);
            // test vite 
            System.Console.WriteLine(pacman.Lives);
            pacman.decreaseLives();
            System.Console.WriteLine(pacman.Lives);
            //test con cambio direzione
            System.Console.WriteLine(pacman.Position);
            pacman.Direction = Direction.DOWN;
            pacman.Position = pacman.nextPosition();
            System.Console.WriteLine(pacman.Position);
        }
    }
}
