﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiacomoProject
{
    class MortalGameObj : MovableGameObj, IMortal
    {
        private int lives;

        protected internal MortalGameObj(int width, int height, Position position, EntityType type, Direction direction, Status status, int speed, int lives) : base(width, height, position, type, direction, status, speed)
        {
            this.lives = lives;
        }

        public virtual bool Dead
        {
            get
            {
                return lives <= 0;
            }
        }

        public virtual void decreaseLives()
        {
            lives = lives - 1;
        }
        public virtual int Lives
        {
            get
            {
                return lives;
            }
        }

    }
}
