﻿using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiacomoProject
{
    class MovableGameObj : SimpleGameObj, IMovable
    {
        private Status status;
        private Direction direction;
        private readonly int speed;

        protected internal MovableGameObj(int width, int height, Position position, EntityType type, Direction direction, Status status, int speed) : base(width, height, position, type)
        {
            this.status = status;
            this.direction = direction;
            this.speed = speed;
        }

        public virtual Direction Direction
        {
            get
            {
                lock (this)
                {
                    return this.direction;
                }
            }
            set
            {
                lock (this)
                {
                    this.direction = value;
                }
            }
        }

        public virtual Status Status
        {
            get
            {
                lock (this)
                {
                    return this.status;
                }
            }
            set
            {
                lock (this)
                {
                    this.status = value;
                }
            }
        }

        public virtual Rectangle getBoundsAt(Position position)
        {
            return new Rectangle(position.X, position.Y, this.Width, this.Height);
        }

        public virtual Position nextPosition()
        {
            switch (this.Direction.innerEnumValue)
            {
                case Direction.InnerEnum.DOWN:
                    return new Position(this.Position.X, this.Position.Y + speed);
                case Direction.InnerEnum.LEFT:
                    return new Position(this.Position.X - speed, this.Position.Y);
                case Direction.InnerEnum.RIGHT:
                    return new Position(this.Position.X + speed, this.Position.Y);
                case Direction.InnerEnum.UP:
                    return new Position(this.Position.X, this.Position.Y - speed);
                default:
                    return new Position(this.Position.X, this.Position.Y);
            }
        }
    }
}
