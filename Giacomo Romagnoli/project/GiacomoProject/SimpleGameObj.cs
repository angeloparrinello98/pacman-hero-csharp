﻿using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiacomoProject
{
    class SimpleGameObj : IEntity
    {

        private readonly int width;
        private readonly int height;
        private Position position;
        private readonly EntityType type;

        protected internal SimpleGameObj(int width, int height, Position position, EntityType type)
        {
            this.width = width;
            this.height = height;
            this.position = position;
            this.type = type;
        }

        public virtual Rectangle Bounds
        {
            get
            {
                return new Rectangle(position.X, position.Y, width, height);
            }
        }

        public virtual Position Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
            }
        }

        public virtual EntityType Type
        {
            get
            {
                return type;
            }
        }

        public virtual int Width
        {
            get
            {
                return width;
            }
        }

        public virtual int Height
        {
            get
            {
                return height;
            }
        }

    }
}
