﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiacomoProject
{
    class Status
    {
        public static readonly Status CHASING = new Status("CHASING", InnerEnum.CHASING);

        public static readonly Status CHASED = new Status("CHASED", InnerEnum.CHASED);

        public static readonly Status CHASED_END = new Status("CHASED_END", InnerEnum.CHASED_END);

        private static readonly List<Status> valueList = new List<Status>();

        static Status()
        {
            valueList.Add(CHASING);
            valueList.Add(CHASED);
            valueList.Add(CHASED_END);
        }

        public enum InnerEnum
        {
            CHASING,
            CHASED,
            CHASED_END
        }

        public readonly InnerEnum innerEnumValue;
        private readonly string nameValue;
        private readonly int ordinalValue;
        private static int nextOrdinal = 0;

        private Status(string name, InnerEnum innerEnum)
        {
            nameValue = name;
            ordinalValue = nextOrdinal++;
            innerEnumValue = innerEnum;
        }

        public static IList<Status> StatusList
        {
            get
            {
                return new List<Status>(Status.values());
            }
        }

        public static Status[] values()
        {
            return valueList.ToArray();
        }

        public int ordinal()
        {
            return ordinalValue;
        }

        public override string ToString()
        {
            return nameValue;
        }

        public static Status valueOf(string name)
        {
            foreach (Status enumInstance in Status.valueList)
            {
                if (enumInstance.nameValue == name)
                {
                    return enumInstance;
                }
            }
            throw new System.ArgumentException(name);
        }
    }
}
