﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project
{
    class EntityType
    {
        public static readonly EntityType WALL = new EntityType("WALL", InnerEnum.WALL, '0');

        public static readonly EntityType PILL = new EntityType("PILL", InnerEnum.PILL, '1');

        public static readonly EntityType POWERPILL = new EntityType("POWERPILL", InnerEnum.POWERPILL, '2');

        public static readonly EntityType PACMAN = new EntityType("PACMAN", InnerEnum.PACMAN, '3');

        public static readonly EntityType BLINKY = new EntityType("BLINKY", InnerEnum.BLINKY, '4');

        public static readonly EntityType PINKY = new EntityType("PINKY", InnerEnum.PINKY, '5');

        public static readonly EntityType INKY = new EntityType("INKY", InnerEnum.INKY, '6');

        public static readonly EntityType CLYDE = new EntityType("CLYDE", InnerEnum.CLYDE, '7');

        public static readonly EntityType EMPTY = new EntityType("EMPTY", InnerEnum.EMPTY, '8');

        private static readonly List<EntityType> valueList = new List<EntityType>();

        static EntityType()
        {
            valueList.Add(WALL);
            valueList.Add(PILL);
            valueList.Add(POWERPILL);
            valueList.Add(PACMAN);
            valueList.Add(BLINKY);
            valueList.Add(PINKY);
            valueList.Add(INKY);
            valueList.Add(CLYDE);
            valueList.Add(EMPTY);
        }

        public enum InnerEnum
        {
            WALL,
            PILL,
            POWERPILL,
            PACMAN,
            BLINKY,
            PINKY,
            INKY,
            CLYDE,
            EMPTY
        }

        public readonly InnerEnum innerEnumValue;
        private readonly string nameValue;
        private readonly int ordinalValue;
        private static int nextOrdinal = 0;
        private readonly char value;

        internal EntityType(string name, InnerEnum innerEnum, char value)
        {
            this.value = value;
            nameValue = name;
            ordinalValue = nextOrdinal++;
            innerEnumValue = innerEnum;
        }

        public char Value
        {
            get
            {
                return this.value;
            }
        }

        public override string ToString()
        {
            switch (this.innerEnumValue)
            {
                case InnerEnum.WALL:
                    return "Wall";
                case InnerEnum.PILL:
                    return "Pill";
                case InnerEnum.POWERPILL:
                    return "PowerPill";
                case InnerEnum.PACMAN:
                    return "Pacman";
                case InnerEnum.INKY:
                    return "Inky";
                case InnerEnum.BLINKY:
                    return "Blinky";
                case InnerEnum.PINKY:
                    return "Pinky";
                case InnerEnum.CLYDE:
                    return "Clyde";
                case InnerEnum.EMPTY:
                    return "Empty";
                default:
                    return "";
            }
        }

        public static EntityType[] values()
        {
            return valueList.ToArray();
        }

        public int ordinal()
        {
            return ordinalValue;
        }

        public static EntityType valueOf(string name)
        {
            foreach (EntityType enumInstance in EntityType.valueList)
            {
                if (enumInstance.nameValue == name)
                {
                    return enumInstance;
                }
            }
            throw new System.ArgumentException(name);
        }
    }
}
