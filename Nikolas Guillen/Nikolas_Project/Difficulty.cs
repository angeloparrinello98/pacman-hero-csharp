﻿using System.Collections.Generic;

namespace Model.Utilities
{
	public sealed class Difficulty
	{
		public static readonly Difficulty NORMAL = new Difficulty("NORMAL", InnerEnum.NORMAL);

		public static readonly Difficulty HARD = new Difficulty("HARD", InnerEnum.HARD);

		public static readonly Difficulty TEST = new Difficulty("TEST", InnerEnum.TEST);

		private static readonly List<Difficulty> valueList = new List<Difficulty>();

		static Difficulty()
		{
			valueList.Add(NORMAL);
			valueList.Add(HARD);
			valueList.Add(TEST);
		}

		public enum InnerEnum
		{
			NORMAL,
			HARD,
			TEST
		}

		public readonly InnerEnum innerEnumValue;
		private readonly string nameValue;
		private readonly int ordinalValue;
		private static int nextOrdinal = 0;

		private Difficulty(string name, InnerEnum innerEnum)
		{
			nameValue = name;
			ordinalValue = nextOrdinal++;
			innerEnumValue = innerEnum;
		}

		public static Difficulty[] values()
		{
			return valueList.ToArray();
		}

		public int ordinal()
		{
			return ordinalValue;
		}

		public static Difficulty valueOf(string name)
		{
			foreach (Difficulty enumInstance in Difficulty.valueList)
			{
				if (enumInstance.nameValue == name)
				{
					return enumInstance;
				}
			}
			throw new System.ArgumentException(name);
		}
	}

}