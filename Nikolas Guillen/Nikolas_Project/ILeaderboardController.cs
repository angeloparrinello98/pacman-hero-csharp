﻿using Model.Utilities;
using System.Collections.Generic;

namespace Controller
{

	public interface ILeaderboardController
	{

		IDictionary<string, int> getSortedRanking(Difficulty difficulty);

		void writeScore(string player, int score, Difficulty difficulty);

		void close();

		void setView(Difficulty difficulty);
	}
}