﻿using Controller.Game;
using Model.Leaderboard;
using System;
using System.Collections.Generic;
using System.Text;

namespace NikolasProject
{
    class Launcher
    {
        static public void Main(String[] args)
        {
			Console.WriteLine("FIRST TEST: LEADERBOARD ORDERING");
            Dictionary<string, int> ranking = new Dictionary<string, int>();
			ranking.Add("Player_1", 3);
			ranking.Add("Player_2", 2);
			ranking.Add("Player_3", 6);
			ranking.Add("Player_4", 5);
			ranking.Add("Player_5", 1);

			Console.WriteLine("\nORIGINAL ORDER:");
			foreach (KeyValuePair<string, int> kvp in ranking)
            {
				Console.WriteLine("\t" + kvp.Key + " " + kvp.Value);
            }

			Console.WriteLine("\n SORTED ORDER:");
			Leaderboard leaderboard = new Leaderboard(ranking);
			String sortedResults = "";
			foreach (KeyValuePair<string, int> kvp in leaderboard.SortedRanking)
			{
				sortedResults = sortedResults + ("\t" + kvp.Key + " " + kvp.Value + "\n");
			}
			Console.Write(sortedResults);

			Console.WriteLine("\n\nLOADMAP TEST:");
			LoadMap loadMap = new LoadMap("standard.txt", Model.Utilities.Difficulty.NORMAL);
			foreach (string entity in loadMap.Entities)
            {
				Console.WriteLine("\t" + entity);
			}
		}
    }
}
