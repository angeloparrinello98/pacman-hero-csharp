﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Model.Leaderboard
{
	public class Leaderboard
	{

		private IDictionary<string, int> ranking;

		public Leaderboard(IDictionary<string, int> ranking)
		{
			this.ranking = ranking;
			sortRanking();
		}

		private void sortRanking()
		{
			Dictionary<string, int> temp = new Dictionary<string, int>();
			foreach (KeyValuePair<string, int> entry in ranking.OrderByDescending(key => key.Value))
			{
				temp.Add(entry.Key, entry.Value);
			}
			this.ranking = new Dictionary<string, int>(temp);
        }

		public virtual IDictionary<string, int> SortedRanking
		{
			get
			{
				return this.ranking;
			}
		}
	}
}