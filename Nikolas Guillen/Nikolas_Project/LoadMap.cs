﻿using Controller.Utilities;
using Model.Utilities;
using System.Collections;
using System.Collections.Generic;
using static Model.Utilities.EntityType;

namespace Controller.Game
{
	public class LoadMap
	{
        private IList<string> entities;
		private Difficulty difficulty;

        public LoadMap(string mapName, Difficulty difficulty)
		{
			entities = new List<string>();
			this.difficulty = difficulty;
			MapIO.readMap(mapName);
			build();
		}

		private void build()
		{
			string current;
			char value;

			for (int i = 0; i < MapIO.Rows; i++)
			{
				current = MapIO.Next;
				for (int j = 0; j < MapIO.Columns; j++)
				{
					value = current[j];
					if (value == WALL.Value)
					{
						entities.Add("WALL");
					}
					if (value == PILL.Value)
					{
						entities.Add("PILL");
					}
					if (value == POWERPILL.Value)
					{
						entities.Add("POWERPILL");
					}
					if (value == PACMAN.Value)
					{
						entities.Add("PACMAN");
					}
					if (value == BLINKY.Value)
					{
						entities.Add("BLINKY");
					}
					if (value == PINKY.Value)
					{
						entities.Add("PINKY");
					}
					if (value == INKY.Value)
					{
						entities.Add("INKY");
					}
					if (value == CLYDE.Value)
					{
						entities.Add("CLYDE");
					}
				}
			}
		}

		public virtual IList<string> Entities
		{
			get
			{
				return this.entities;
			}
		}
	}

}