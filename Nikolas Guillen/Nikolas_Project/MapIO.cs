﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Controller.Utilities
{
	public sealed class MapIO
	{
		private static int rows;
		private static int columns;
		private static StreamReader content;
		private static readonly char SEP = System.IO.Path.DirectorySeparatorChar;
		private static readonly string PATH = "Maps" + SEP;
		private const int SCALE = 30;

		private MapIO()
		{
		}

		public static void readMap(string mapName)
		{
			content = new StreamReader(PATH + mapName);
			columns = int.Parse(Next);
			rows = int.Parse(Next);
		}

		public static int Rows
		{
			get
			{
				return rows;
			}
		}

		public static int Columns
		{
			get
			{
				return columns;
			}
		}

		public static string Next
		{
			get
			{
				return content.ReadLine();
			}
		}
	}

}